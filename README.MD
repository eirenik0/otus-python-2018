# Python Developer course. 2018-08
Code samples, study materials, etc.

### Author
Serhii Khalymoon

slack: Serhii Khalymon

email: sergiykhalimon@gmail.com


### Home works
1. [Log analyzer](https://gitlab.com/Infernion/otus-python-2018/tree/master/01_advanced_basics/log_analyzer)
