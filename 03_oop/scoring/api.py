#!/usr/bin/env python
# -*- coding: utf-8 -*-

import abc
import json
import datetime as dt
import logging
import hashlib
import types
import uuid
import weakref
from optparse import OptionParser
from http.server import HTTPServer, BaseHTTPRequestHandler

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class Field(abc.ABC):
    type: None

    def __init__(self, required, nullable=False, default=None):
        self._required = required
        self._nullable = nullable
        self._default = default
        self._values = weakref.WeakKeyDictionary()

    def __get__(self, instance, owner):
        return self._values.get(instance, self._default)

    def __set__(self, instance, value):
        self._validate(value)
        self._values[instance] = value

    def _validate(self, value):
        if self._required and value is None and not self._nullable:
            raise ValueError("The field is required.")

        type = getattr(self.__annotations__, 'type')
        if not isinstance(value, type):
            raise ValueError(f"Field must has type {type}")




class CharField(Field):
    type: str


class ArgumentsField(Field):
    type: list


class EmailField(CharField):
    pass


class PhoneField(Field):
    type: str


class DateField(Field):
    type: dt.datetime


class BirthDayField(Field):
    type: dt.datetime


class GenderField(Field):
    type: str


class ClientIDsField(Field):
    type: list


class AttrsToInitMetaclass(type):
    def __new__(meta, clsname, bases, dct):
        cls_attrs = {attr: None
                     for attr, obj in dct.items()
                     if not attr.startswith('__') and isinstance(obj, Field)}

        # def _init(self, **cls_attrs):
        #     for attr, val in cls_attrs.items():
        #         setattr(self, attr, val)

        # meta._fields = cls_attrs.values()
        # meta.__init__ = types.MethodType(_init, meta)
        newclass = super(AttrsToInitMetaclass, meta).__new__(meta, clsname, bases, dct)
        setattr(newclass, '_fields', cls_attrs.keys())
        return newclass


class BaseRequest(metaclass=AttrsToInitMetaclass):
    def validate(self):
        # if not bool(self):
            # raise ValueError(f"The {self.__class__.__name__} is empty.")

        errors = []
        for attr in self._fields:
            try:
                getattr(self, attr)._validate()
            except ValueError as e:
                errors.append(str(e))
            except AttributeError:

                import ipdb;ipdb.set_trace()
        if errors:
            raise ValueError("Some errors had appeared during validation: \n\t {}".format('\n\t'.join(errors)))

    @property
    def is_valid(self):
        try:
            self.validate()
            return True
        except ValueError as e:
            logging.error(str(e))
            return False

    def from_json(self, data):
        if isinstance(data, str):
            data = json.loads(data)
        for key, value in data.items():
            if key not in self._fields:
                logging.debug(f"Bad key passed.")
                continue
            setattr(self, key, value)
        return self

    def __bool__(self):
        return all([getattr(self, f) for f in self._fields])


class ClientsInterestsRequest(BaseRequest):
    client_ids = ClientIDsField(required=True)
    date = DateField(required=False, nullable=True)


class OnlineScoreRequest(BaseRequest):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)


class MethodRequest(BaseRequest):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN

    def build_requested_method(self):
        cls = None
        if self.method == 'online_score':
            cls = OnlineScoreRequest()
        elif self.method == 'clients_interests':
            cls = ClientsInterestsRequest()

        if cls:
            return cls.from_json(self.arguments)


def is_authorized(request):
    if request.is_admin:
        digest = hashlib.sha512(dt.datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT).hexdigest()
    else:
        digest = hashlib.sha512(request.account + request.login + SALT).hexdigest()
    if digest == request.token:
        return True
    return False


def online_score(): pass
def clients_interests(): pass


def get_method(request):
    methods = dict(online_score=online_score)
    try:
        method = methods[request['method']]
    except KeyError:
        method = None
    return method


def get_wrapped_request(request):
    return MethodRequest(**request)

def method_handler(request, ctx, store):
    request = MethodRequest().from_json(request)

    response, code = None, None
    if not request.is_valid:
        return response, INVALID_REQUEST
    if not request.token or not is_authorized(request):
        return response, FORBIDDEN

    # request = get_wrapped_request(request)
    return response, code


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    store = None

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path]({"body": request, "headers": self.headers}, context, self.store)
                except Exception as e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(code, "Unknown Error"), "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r))
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
